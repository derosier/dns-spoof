//****************************************************************************
// Module name : CError
// Project     : dns-spoof
//
// Copyright (C) 2010 by Steve deRosier.
// All rights reserved.
//
// The information contained herein is confidential 
// property of Company. The use, copying, transfer or 
// disclosure of such information is prohibited except
// by express written agreement with Company.
//
// First written on 1/22/2010 by Steve deRosier.
//
// Module Description:
//   Error object for try/catch reporting.
//
//****************************************************************************
//  $Id$
//****************************************************************************

//****************************************************************************
//  Include section
// Add all #includes here
//
//****************************************************************************
#include "cerror.h"

//****************************************************************************
//  Defines section
// Add all #defines or consts here
//
//****************************************************************************
std::string UDP_ErrorStrings[CE_UDPSocket::MAX_ERROR] =
{  
   "Unknown failure",
   "Can't create socket",
   "Can't bind socket", 
   "Error on close socket",
   "Socket not open",
   "Error on receive",
   "Error on send",
   "Wrong socket type",
   "Message size is zero",
   "Message destination is not set"
};

//****************************************************************************
//  Function Prototype Section
// Add prototypes for all functions called by this
// module, with the exception of runtime routines,
// or prototypes already included via header files.
//****************************************************************************

//****************************************************************************
// Function name: TYPE foo(TYPE arg1, TYPE arg2)
//    returns: return value description
//    arg1: description
//    arg2: description
// Created by: author's name
// Date created: date
// Description: detailed description 
// Notes: restrictions, odd modes
//****************************************************************************


