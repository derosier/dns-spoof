/*****************************************************************************
 * Version Module - dns-spoof
 *
 * Copyright (C) 2010 Steve deRosier.
 * All rights reserved.
 *
 * The information contained herein is confidential  property of Cal-Sierra 
 * Communications. The use, copying, transfer or disclosure of such information 
 * is prohibited except by express written agreement with Cal-Sierra 
 * Communications.
 *
 *****************************************************************************/
/*****************************************************************************
 * When you do a full new version release you 
 * should modify this to indicate what the new
 * version is.  Keep  a full history.  If there 
 * is a ROM ID number or a part number, put it in
 * here.  If this version has been tagged in CVS
 * (which it should have been), enter the tag 
 * in here.  
 * 07/12/97 - Version 0.50 - ROM ID 0000
 *            Initial stub version
 *            CVS Tag: none
 *****************************************************************************/
/*  $Id$
 *****************************************************************************/

#undef _VERSION
#define _VERSION "Version 1.00"
#undef _COPYRIGHT
#define _COPYRIGHT "Copyright (C) 2010 Steve deRosier. All Rights Reserved."
#undef _PROJECT
#define _PROJECT "dns-spoof"

